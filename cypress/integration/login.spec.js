/// <reference types="cypress" />

describe('Try to login at adopets.com with invalid credentials', () => {
  before(() => {
    cy.visit('https://adopets.com')
  })

  it('should visit adopets.com', () => {
    cy.url().should('contain', 'adopets.com')
  })

  it('should click in Login and visit /login', () => {
    cy.scrollTo(0, 1000)
    cy.contains('Login').click()
    cy.url().should('contain', 'login')
  })

  it('should type the email and password then click in login', () => {
    const user = {
      email: 'user@user.com',
      password: 'password'
    }

    cy.get('input[name=loginEmail]')
      .type(user.email)
      .should('contain.value', user.email)

    cy.get('input[name=loginPassword]')
      .type(user.password)
      .should('contain.value', user.password)

    cy.get('.login-form-button').click()
  })

  it('should receive a error message (not registered)', () => {
    cy.get('.message').should('contain.text', 'not registered')
  })
})

describe('Just to throw an error', () => {
  it('should access google.com', () => {
    cy.visit('https://adopets.com')
    cy.url().should('contain', 'google')
  })
})
