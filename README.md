# Automatized tests with Cypress

---

## How to run?

You only need [Node.js](https://nodejs.org/en/) installed

1. First, install the dependencies running `npm install`
2. Then, `npm start` and choose the test file that you want

## Headless mode

If you don't want to run in interactive mode, just run `npm run cy:run`.

If any test fail, cypress will generate a screenshot and a video at `cypress/screenshots` and `cypress/videos` directories.
